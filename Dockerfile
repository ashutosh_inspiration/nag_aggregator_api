FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY ["Nag.Aggregator.Service/Nag.Aggregator.Service.csproj", "Nag.Aggregator.Service/"]
COPY ["Nag.Users.Core/Nag.Users.Core.csproj", "Nag.Users.Core/"]
RUN dotnet restore "Nag.Aggregator.Service/Nag.Aggregator.Service.csproj"
COPY . .

WORKDIR "/app/Nag.Aggregator.Service"

RUN dotnet publish "Nag.Aggregator.Service.csproj" -c Release -o /app/out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
EXPOSE 80/tcp
EXPOSE 443/tcp
EXPOSE 8093
EXPOSE 8093/tcp
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Nag.Aggregator.Service.dll"]