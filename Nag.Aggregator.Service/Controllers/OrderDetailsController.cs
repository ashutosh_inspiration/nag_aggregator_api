﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nag.Users.Core.DTOs;
using Newtonsoft.Json;
using Steeltoe.Common.Discovery;

namespace Nag.Aggregator.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderDetailsController : ControllerBase
    {
        
        public OrderDetailsController(IConfiguration config)
        {
            Config = config;
        }

        public IConfiguration Config { get; }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var client = new HttpClient();
            var userResult = await client.GetAsync(Config.GetValue<string>("UsersServiceUrl") + id);

            var user = JsonConvert.DeserializeObject<UserDto>(await userResult.Content.ReadAsStringAsync());
            if (user == null) {
                return NotFound(); ;
            }
            var ordersResult = await client.GetAsync(Config.GetValue<string>("OrdersServiceUrl") + id);

            var orders = JsonConvert.DeserializeObject<List<OrderDto>>(await ordersResult.Content.ReadAsStringAsync());

            var result = new OrderDetailDto()
            {
                UsersDeatil = user,
                Orders = orders != null ? orders : new List<OrderDto>()
            };

            return Ok(result);
        }
       
    }
}
