﻿using Microsoft.EntityFrameworkCore;
using Nag.Users.Core.Models;

namespace Nag.Users.Data
{
    public interface IAppDataContext
    {
        DbSet<Order> Orders { get; set; }
        DbSet<User> Users { get; set; }

        void SeedData();
    }
}