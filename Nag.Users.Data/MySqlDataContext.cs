﻿using Microsoft.EntityFrameworkCore;
using Nag.Users.Core.Models;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Linq;

namespace Nag.Users.Data
{
    public class MySqlDataContext : DbContext, IAppDataContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public MySqlDataContext(DbContextOptions options) : base(options)
        {
            SeedData();
        }

        public void SeedData()
        {
            if (!Users.Any())
            {
                Users.Add(new User() { Id = 1, Age = 28, Name = "John", Email = "jondoe@gmail.com" });
                Users.Add(new User() { Id = 2, Age = 28, Name = "Jakub", Email = "jakub@gmail.com" });
                Orders.Add(new Order() { Id = 1, Amount = 250, Date = new DateTime(2020, 5, 10), UserId = 1 });
                Orders.Add(new Order() { Id = 2, Amount = 256, Date = new DateTime(2020, 5, 12), UserId = 1 });
                Orders.Add(new Order() { Id = 3, Amount = 254, Date = new DateTime(2020, 5, 1), UserId = 1 });
                Orders.Add(new Order() { Id = 4, Amount = 251, Date = new DateTime(2020, 5, 17), UserId = 2 });

                SaveChanges();
            }
        }
    }
}
