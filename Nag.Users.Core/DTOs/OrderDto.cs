﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nag.Users.Core.DTOs
{
    public class OrderDto
    { 
        public int Id { get; set; }
        public int UserId { get; set; }
        public int Ammount { get; set; }
        public DateTime Date { get; set; }

    }
}
